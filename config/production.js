// production configuration

module.exports = {
    environment: 'production',
    server: {
        hostName: '', //Need production URL
        port: 8888
    },
    dbConfig: {
        connectionLimit : 100, //important
        host     : 'localhost',
        user     : 'root',
        password : 'root',
        database : 'bookapp_db',
        debug    :  false
    },
    log: {
        name: 'cafe-laundry',
        path: 'cafe-laundry.log',
        consoleLevel: 'debug',
        fileLevel: 'trace',
        backups: 3
    },
    textLocal:{
        host:"http://api.textlocal.in/send?",
        email:"sharad.is.biradar@gmail.com",
        apiKey:"6PYTszR+5+8-MV53IxiyISlLvX484Kl3Ial7FUTeQN"
    },
    emailList:{
        fromEmail:"sharad.is.biradar@gmail.com",
        toEmail:"suvarnadjagtap@gmail.com, sharad.is.biradar@gmail.com, hanmanthpatil@gmail.com"
    },
    smtp:{
       host: 'smtp.gmail.com',
        port: 465,
        secureConnection : false,
        auth: {
            user: 'sharad.is.biradar@gmail.com',
            pass: '123@1234'
        }   
    },
    jwt:{
        secret:'2d034746-1g04-1a4h-4b5g-34f5g8h803r8'
    }
};
